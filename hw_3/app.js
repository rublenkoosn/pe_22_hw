let number = '';
let result = '';
let i = 0;

while (!Number(number) || number === null || !Number.isInteger(number)) {
    number = +prompt('Enter number');
}

do {
    if (i % 5 === 0 && number >= 5) {
        result += i;
        console.log(i);
    }
    i++;
} while (i <= number)

if (number < 0 || +result === 0) {
    alert('Sorry, no numbers');
}