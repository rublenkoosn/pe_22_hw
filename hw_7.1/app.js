let userName = prompt('Enter first name');
let userLastName = prompt('Enter last name');
let student = {
    firstName: userName,
    lastName:  userLastName,
    table: {},
    getBadPoint: function () {
        let tableLessons = this.table;
        let badPoint = 0;

        for (let item in tableLessons) {
            if (Number(tableLessons[item]) < 4) {
                badPoint++
            }
        }

        if (badPoint !== 0) {
            console.log(`Студента не переведено на наступний курс`)
            return false;
        }
        alert('Студент переведено на наступний курс');
    },
    GPA: function () {
        let tableLessons = this.table;
        let sumPoint = 0;
        let i = 0;
        let gpaPoint = 0

        for (let lesson in tableLessons) {
            sumPoint += Number(tableLessons[lesson]);
            i++;
        }
        gpaPoint = gpaPoint / i;

        if (gpaPoint < 7) {
            console.log(`Незадовільний середній бал ${gpaPoint}`)
            return false;
        }
        alert('Студенту призначено стипендію')
    }
};

let lesson = '';
let point = '';

while (lesson !== null ) {
    lesson = prompt('Enter lesson');
    if (lesson === null) {
        break;
    }
    point = prompt('Enter point to lesson');
    student.table[`${lesson}`] = point;
}

console.log(student.getBadPoint());
console.log(student.GPA());


