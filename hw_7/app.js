'use strict';

function createUser() {
    let promptUserName = prompt('Enter your name');
    let promptUserLastName = prompt('Enter your last name');
    let newUser = {
        firstName: promptUserName,
        lastName: promptUserLastName,
        getLogin: function () {
            let login = this.firstName[0] + this.lastName;
            return login.toLowerCase();
        }
    }

    return newUser.getLogin();
}

console.log(createUser())