'use strict';

let firstNumber = prompt('Enter first number');
let secondNumber = prompt('Enter second number')
let mathSymbol = '';

while (
        (firstNumber === null || secondNumber === null) ||
        (firstNumber === '' || secondNumber === '') ||
        (!Number(firstNumber) || !Number(secondNumber))
    ) {
    firstNumber = prompt('Enter first number', firstNumber);
    secondNumber = prompt('Enter second number', secondNumber);
}

while (mathSymbol === '' || mathSymbol == null) {
    mathSymbol = prompt('Enter math operation');
}

function calcNumber(num1, num2, operation) {
    let result;

    (operation === '-') ? result = num1 - num2 :
        (operation === '+') ? result = num1 + num2 :
        (operation === '*') ? result = num1 * num2 :
        (operation === '/') ? result = num1 / num2 : '';

    return  console.log(result);
}

calcNumber(Number(firstNumber), Number(secondNumber), mathSymbol);

